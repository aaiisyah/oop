<?php
require_once 'animal.php';
require_once 'frog.php';
require_once 'ape.php';

$sheep = new Animal("shaun");

echo $sheep->get_name(); // "shaun"
echo "<br>";
echo $sheep->get_legs(); 
echo "<br>";// 2
echo $sheep->get_coldblooded(); // false
echo "<br><br>";

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

$sungokong = new Ape("kera sakti");
echo $sungokong->get_name(); // kera sakti
echo "<br>";
echo $sungokong->get_legs();
echo "<br>";
echo $sungokong->get_coldblooded(); // false
echo "<br>";
$sungokong->yell(); // "Auooo"
echo "<br><br>";


$kodok = new Frog("buduk");
echo $kodok->get_name(); // buduk
echo "<br>";
echo $kodok->get_legs();
echo "<br>";
echo $kodok->get_coldblooded(); // true
echo "<br>";
$kodok->jump() ; // "hop hop"




?>